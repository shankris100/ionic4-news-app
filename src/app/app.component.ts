import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Business',
      url: '/business',
      icon: 'briefcase'
    },
      {
          title: 'Entertainment',
          url: '/entertainment',
          icon: 'film'
      },
      {
          title: 'Health',
          url: '/health',
          icon: 'heart'
      },
      {
          title: 'Science',
          url: '/science',
          icon: 'planet'
      },
      {
          title: 'Sports',
          url: '/sports',
          icon: 'football'
      },
      {
          title: 'Technology',
          url: '/technology',
          icon: 'pulse'
      }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
