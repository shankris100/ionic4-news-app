import {Component, OnInit} from '@angular/core';
import {NewsService} from '../services/news.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  newsArray: any = [];
  constructor(private news: NewsService, private router: Router) {
  }

  ngOnInit(): void {
    this.loadHeadLines();
  }

  loadHeadLines() {
    this.news.getNews().subscribe(news => {
      this.newsArray = news['articles'];
      console.log(this.newsArray);
    });
  }

    getDetails(news) {
      this.router.navigate(['/newsdetail', { 'title': news.title, 'desc': news.description, 'img': news.urlToImage, 'url': news.url }]);
    }
}
