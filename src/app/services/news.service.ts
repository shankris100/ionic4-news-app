import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  public baseUrl: any = 'https://newsapi.org/v2/';
  public country: any = 'country=in';
  public apiKey: any = '&apiKey=9bd6aa4afb8145b088f25ee12713a1bf';
  constructor(private http: HttpClient) { }

  getNews() {
    const url = this.baseUrl + 'top-headlines?' + this.country + this.apiKey;
    return this.http.get(url);
  }

  getNewbyCategory(category) {
    const url = this.baseUrl + 'top-headlines?' + this.country + '&category=' + category + this.apiKey;
    return this.http.get(url);
  }
}
